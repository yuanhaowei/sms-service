package cn.pb.sms.bean;


/**
 * 【请填写功能名称】对象 users
 * 
 * @author liuwei
 * @date 2021-01-30
 */
public class Sensor {
    private static final long serialVersionUID = 1L;

    /** time */
    private String time;

    /** name */
    private String name;

    /** floor */
    private Integer floor;

    /** id */
    private String id;

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getTime()
    {
        return time;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setFloor(Integer floor)
    {
        this.floor = floor;
    }

    public Integer getFloor()
    {
        return floor;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    @Override
    public String toString() {
        return "Users{" +
                "time='" + time + '\'' +
                ", name='" + name + '\'' +
                ", floor=" + floor +
                ", id='" + id + '\'' +
                '}';
    }
}
