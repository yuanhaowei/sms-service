package cn.pb.sms.bean.vo;

import cn.pb.sms.bean.Sensor;

import java.util.List;

/**
 * @author yuan
 * @date 2021/1/30 21:07
 */
public class SensorVo {
    List<Sensor> sensors;

    public List<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    @Override
    public String toString() {
        return "SensorVo{" +
                "sensors=" + sensors +
                '}';
    }
}
