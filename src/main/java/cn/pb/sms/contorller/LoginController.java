package cn.pb.sms.contorller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;

/**
 * @author 裴波
 * @date 2021/1/31 16:46
 */
@Controller
public class LoginController {
    /**
     * 获取登录页面
     * @param model
     * @return
     */
    @RequestMapping("/login")
    public String login (Model model,String msg){
        model.addAttribute("msg",msg);
        return "login";
    }

    /**
     * 做登录逻辑
     * @param model
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("/dologin")
    public String doLogin (Model model, String username, String password, HttpServletRequest request){
        String msg = "";
        if("admin".equals(username)){
            if ("admin123".equals(password)){
                request.getSession(true).setAttribute(username,username);
                return "redirect:/index";
            }else {
                msg = "密码错误";
            }
        }else {
            msg = "用户名不存在";
        }
        return "redirect:/login?msg="+ URLEncoder.encode(msg);

    }
}
