package cn.pb.sms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 裴波
 * @date 2021/1/30 18:30
 */
@SpringBootApplication
@MapperScan("cn.pb.sms.mapper")
public class StarterSMS {
    public static void main(String[] args) {
        SpringApplication.run(StarterSMS.class,args);
    }
}
