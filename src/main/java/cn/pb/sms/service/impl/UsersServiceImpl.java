package cn.pb.sms.service.impl;


import cn.pb.sms.service.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author liuwei
 * @date 2021-01-30
 */
@Service
public class UsersServiceImpl implements IUsersService
{
//    @Autowired
//    private UsersMapper usersMapper;
//
//    /**
//     * 查询【请填写功能名称】
//     *
//     * @param time 【请填写功能名称】ID
//     * @return 【请填写功能名称】
//     */
//    @Override
//    public Users selectUsersById(String time)
//    {
//        return usersMapper.selectUsersById(time);
//    }
//
//    /**
//     * 查询【请填写功能名称】列表
//     *
//     * @param users 【请填写功能名称】
//     * @return 【请填写功能名称】
//     */
//    @Override
//    public List<Users> selectUsersList(Users users)
//    {
//        return usersMapper.selectUsersList(users);
//    }
//
//    /**
//     * 新增【请填写功能名称】
//     *
//     * @param users 【请填写功能名称】
//     * @return 结果
//     */
//    @Override
//    public int insertUsers(Users users)
//    {
//        return usersMapper.insertUsers(users);
//    }
//
//    /**
//     * 修改【请填写功能名称】
//     *
//     * @param users 【请填写功能名称】
//     * @return 结果
//     */
//    @Override
//    public int updateUsers(Users users)
//    {
//        return usersMapper.updateUsers(users);
//    }
//
//    /**
//     * 删除【请填写功能名称】对象
//     *
//     * @param ids 需要删除的数据ID
//     * @return 结果
//     */
//    @Override
//    public int deleteUsersByIds(String ids)
//    {
//        return usersMapper.deleteUsersByIds(Convert.toStrArray(ids));
//    }
//
//    /**
//     * 删除【请填写功能名称】信息
//     *
//     * @param time 【请填写功能名称】ID
//     * @return 结果
//     */
//    @Override
//    public int deleteUsersById(String time)
//    {
//        return usersMapper.deleteUsersById(time);
//    }
}
