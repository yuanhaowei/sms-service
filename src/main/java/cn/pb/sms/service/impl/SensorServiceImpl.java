package cn.pb.sms.service.impl;

import cn.pb.sms.bean.Sensor;
import cn.pb.sms.bean.vo.SensorVo;
import cn.pb.sms.mapper.SensorMapper;
import cn.pb.sms.service.SensorService;
import cn.pb.sms.utils.FastJsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 裴波
 * @date 2021/1/30 19:00
 */
@Service
public class SensorServiceImpl implements SensorService {

    @Autowired(required = false)
    private SensorMapper sensorMapper;

    public SensorVo querySensorInfo() {
        List<Sensor> sensors = sensorMapper.selectSensorList();
        SensorVo sensorVo = new SensorVo();
        sensorVo.setSensors(sensors);
        return sensorVo;
    }

    public void insertSensorInfo(String jsonObj) {
        Sensor sensor = FastJsonUtils.convertObject(jsonObj,Sensor.class);
        sensorMapper.insertUsers(sensor);
    }

    public void deleteSensorInfo(String delid) {
        sensorMapper.deleteUsersById(delid);
    }

    public String getEditInfo(String editid) {
        Sensor sensor = sensorMapper.selectUsersById(editid);
        return FastJsonUtils.toString(sensor);
    }
}
