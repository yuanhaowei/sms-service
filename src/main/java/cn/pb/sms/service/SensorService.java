package cn.pb.sms.service;

import cn.pb.sms.bean.Sensor;
import cn.pb.sms.bean.vo.SensorVo;

import java.util.List;

/**
 * @author 裴波
 * @date 2021/1/30 19:00
 */
public interface SensorService {

    SensorVo querySensorInfo();

    void insertSensorInfo(String jsonObj);

    void deleteSensorInfo(String delid);

    String getEditInfo(String editid);
}
