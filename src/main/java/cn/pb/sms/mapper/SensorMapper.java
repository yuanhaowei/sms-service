package cn.pb.sms.mapper;

import cn.pb.sms.bean.Sensor;
import cn.pb.sms.bean.vo.SensorVo;

import java.util.List;

/**
 * @author yuan
 * @date 2021/1/30 19:33
 */
public interface SensorMapper {
    /**
     * 查询【请填写功能名称】
     *
     * @param time 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public Sensor selectUsersById(String time);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param users 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Sensor> selectUsersList(Sensor users);

    /**
     * 新增【请填写功能名称】
     *
     * @param users 【请填写功能名称】
     * @return 结果
     */
    public int insertUsers(Sensor users);

    /**
     * 修改【请填写功能名称】
     *
     * @param users 【请填写功能名称】
     * @return 结果
     */
    public int updateUsers(Sensor users);

    /**
     * 删除【请填写功能名称】
     *
     * @param time 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteUsersById(String time);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param times 需要删除的数据ID
     * @return 结果
     */
    public int deleteUsersByIds(String[] times);


    List<Sensor> selectSensorList();
}
